import { RootState } from '../rootReducer';

const symptomsConfigs = (state: RootState) => state.symptomsConfigs;

export {
  symptomsConfigs
};