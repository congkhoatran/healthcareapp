import { Symptom } from './models/symptom';
import { AppThunk } from '../store';

import {
  loadSymptoms,
  updateSymptom
} from './symptomsSlice';

export const loadAllSymptoms = (symptoms: Symptom[]): AppThunk<void> =>
  async (dispatch) => {
    dispatch((loadSymptoms(symptoms)));
  };

export const updateCurrentSymptom = (symptoms: Symptom[]): AppThunk<void> =>
  async (dispatch) => {
    dispatch(updateSymptom(symptoms));
  };
