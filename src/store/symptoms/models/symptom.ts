export interface Symptom {
  name: string;
  isSelected: boolean;
}