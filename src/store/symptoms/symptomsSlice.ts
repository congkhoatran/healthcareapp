import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Symptom } from './models/symptom';

const symptomSlice = createSlice({
  name: 'symptomSlice',
  initialState: {
    symptoms: [] as Symptom[]
  },
  reducers: {
    loadSymptoms: (state, action: PayloadAction<Symptom[]>) => {
      state.symptoms = action.payload;
    },
    updateSymptom: (state, action: PayloadAction<Symptom[]>) => {
      state.symptoms = action.payload;
    }
  }
});

export const {
  loadSymptoms,
  updateSymptom
} = symptomSlice.actions;

export default symptomSlice.reducer;
