import { combineReducers } from 'redux';
import symptomsReducer from './symptoms/symptomsSlice';

const rootReducer = combineReducers({
  symptomsConfigs: symptomsReducer
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;