import React from 'react';
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory, History } from 'history';
import { Provider } from 'react-redux';
import routers_string from './utilities/routers';
import store from '../store/store';
const HomePage = React.lazy(() => import('./pages/homePage'));
const BookingPage = React.lazy(() => import('./pages/bookingPage'));

export const history: History = createBrowserHistory();

const App: React.SFC = () => (
  <Router history={history}>
    <React.Suspense fallback="loading...">
      <Provider store={store}>
        <Switch>
          <Route exact={true} path={routers_string.HOME} component={HomePage} />
          <Route exact={true} path={routers_string.BOOKING} component={BookingPage} />
        </Switch>
      </Provider>
    </React.Suspense>
  </Router>
);

export default App;
