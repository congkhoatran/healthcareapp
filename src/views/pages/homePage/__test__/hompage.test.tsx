import { render, screen } from '@testing-library/react';
import HomePage from '../index';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch
}));

test('Render Snapshot!', () => {
  const homePage = render(<HomePage />).container;
  expect(homePage).toMatchSnapshot();
});

test('Book a doctor now!', () => {
  render(<HomePage />);
  const text = screen.getByText('Book a doctor now!');
  expect(text).toBeInTheDocument();
});
