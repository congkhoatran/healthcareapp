import { useHistory } from 'react-router-dom';
import * as Styled from './styles';
import routers_string from '../../utilities/routers';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { AppDispatch } from '../../../store/store';
import { loadAllSymptoms } from '../../../store/symptoms/symptomsActions';
import { Symptom } from '../../../store/symptoms/models/symptom';

const Home: React.FC = () => {
  const history = useHistory();
  const dispatch: AppDispatch = useDispatch();

  const goToBooking = () => {
    history.push(routers_string.BOOKING);
  };

  useEffect(() => {
    let symptoms: Symptom[] = [];
    for (let index = 0; index < 20; index++) {
      const symptom: Symptom = {
        name: `Symptom ${index}`,
        isSelected: index > 3 ? false : true,
      };
      symptoms.push(symptom);
    }
    dispatch(loadAllSymptoms(symptoms));
  }, [dispatch])

  return (
    <Styled.HomeContainer>
      <Styled.BookingButton onClick={goToBooking}>Book a doctor now!</Styled.BookingButton>
    </Styled.HomeContainer>
  )
};

export default Home as React.ComponentType;