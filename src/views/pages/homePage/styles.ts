import styled from 'styled-components';

export const HomeContainer = styled.div`
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
`;

export const BookingButton = styled.button`
  font-size: 20px;
  border: none;
  background-color: transparent;
`;
