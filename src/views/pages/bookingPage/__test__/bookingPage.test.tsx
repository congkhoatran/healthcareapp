import { cleanup, render } from '@testing-library/react';
import BookingPage from '../index';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from '../../../../store/rootReducer';

describe('Booking Page', () => {
  beforeAll(function () {
    Object.defineProperty(window, 'matchMedia', {
      value: () => {
        return {
          matches: false,
          addListener: () => { },
          removeListener: () => { }
        };
      }
    })
    jest.mock('react-redux', () => {
      return {
        ...jest.requireActual('react-redux') as {},
        useSelector: jest
          .fn()
          .mockReturnValue({
            symptoms: []
          })
      };
    });
  });
  afterEach(function () {
    cleanup();
  });
  test('Render Snapshot!', () => {
    const store = createStore(rootReducer, {});
    const homePage = render(<Provider store={store}><BookingPage /></Provider>).container;
    expect(homePage).toMatchSnapshot();
  });
});



