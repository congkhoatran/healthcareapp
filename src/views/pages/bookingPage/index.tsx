import React from 'react';
import { GlobalStyled } from '../../components/GlobalStyled';

import * as Styled from './styles';

import BookingTabs from '../../components/BookingTabs';
import BookingHeader from '../../components/BookingHeader';
import BookingPatients from '../../components/Patients';
import ReasonAndBookingTime from '../../components/ReasonAndBookingTime';
import SymptomAndReasons from '../../components/SymptomAndReasons';
import Button from '../../components/Button';

const BookingPage: React.FC = () => (
  <Styled.Layout>
    <GlobalStyled />
    <BookingHeader />
    <Styled.ContentContainer>
      <BookingTabs />
      <BookingPatients />
      <ReasonAndBookingTime />
      <SymptomAndReasons />
    </Styled.ContentContainer>
    <Button label='Next'/>
  </Styled.Layout>
);

export default BookingPage as React.ComponentType;
