import WebFontLoader from 'webfontloader';
import { createGlobalStyle } from 'styled-components';

WebFontLoader.load({
  google: {
    families: ['IBM Plex Sans Condensed:400,500,600,700'],
  },
});

export const GlobalStyled = createGlobalStyle`
  {
    margin: 0;
    padding: 0;
  }

  body {
    font-family: 'IBM Plex Sans Condensed', serif;
    background-color: #FFFFFF;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
`;
