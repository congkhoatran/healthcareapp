import styled from 'styled-components';

export const Container = styled.div`

`

export const SelectedSymptomAndReasonText = styled.p`
  font-size: 1.35em;
  color: #394656;
  padding: 20px 0 0 10px;
  font-weight: 600;
  font-family: sans-serif;
`

export const ChooseSymptomAndReasonText = styled.p`
  font-size: 1.35em;
  color: #394656;
  padding: 20px 0 0 10px;
  font-weight: 600;
  font-family: sans-serif;
`

export const SelectedSymptomAndReasonContainer = styled.div`
  padding: 0 0 0 10px;
`

export const ChooseSymptomAndReasonContainer = styled.div`
  padding: 0 0 0 10px;
`