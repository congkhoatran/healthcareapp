import React from 'react';
import { useSelector } from 'react-redux';
import * as Styled from './styles';
import {
  symptomsConfigs
} from '../../../store/symptoms/selectors';
import ReasonButton from '../../components/ReasonButton';

const SymptomAndReasons: React.FC = () => {

  const symptoms = useSelector(symptomsConfigs).symptoms;

  return (<Styled.Container>
    <Styled.SelectedSymptomAndReasonText>
      Selected symptom and reasons:
    </Styled.SelectedSymptomAndReasonText>
    <Styled.SelectedSymptomAndReasonContainer>
      {symptoms.map((item, index) => {
        return item.isSelected && (<ReasonButton key={`${item.name}_${index}`} label={item.name} isSelected={item.isSelected} />)
      })
      }
    </Styled.SelectedSymptomAndReasonContainer>
    <Styled.ChooseSymptomAndReasonText>
      Choose your symptoms and reasons:
    </Styled.ChooseSymptomAndReasonText>
    <Styled.ChooseSymptomAndReasonContainer>
      {symptoms.map((item, index) => {
        return !item.isSelected && (<ReasonButton key={`${item.name}_${index}`} label={item.name} isSelected={item.isSelected} />)
      })
      }
    </Styled.ChooseSymptomAndReasonContainer>
  </Styled.Container>);
};

export default SymptomAndReasons as React.ComponentType;