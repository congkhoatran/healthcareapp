import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 10px 0 10px;
  justify-content: space-between;
  height: 100%;
`

export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  padding: 0 10px 0 10px;
`

export const Title = styled.p`
  font-size: 1.35em;
  font-weight: bold;
  font-family: sans-serif;
  color: #394656;
  margin-bottom: 0;
`

export const SubTitle = styled.p`
  font-size: 1.35em;
  font-family: sans-serif;
  color: #394656;
`

export const ExampleSelection = styled.div`
  background-color: #ebf4fe;
  box-shadow: inset 5px 5px 5px 0px #d2dfef;
  height: 50px;
  border-radius: 10px;
  margin-bottom: 20px;
  padding: 10px;
  font-size: 1.35em;
  font-weight: 600;
  color: #b9c4d2;
`

export const SelectedSymptomsText = styled.p`
  font-size: 1.35em;
  font-weight: bold;
  font-family: sans-serif;
  color: #394656;
`

export const ChooseYourSymptomsText = styled.p`
  font-size: 1.35em;
  font-weight: bold;
  font-family: sans-serif;
  color: #394656;
`

export const SelectedSymptomAndReasonContainer = styled.div`
  padding: 0 0 20px 10px;
`

export const ChooseSymptomAndReasonContainer = styled.div`
  padding: 0 0 20px 10px;
`