import React from 'react';
import * as Styled from './styles';
import Sheet from 'react-modal-sheet';
import Button from '../Button';
import ReasonButton from '../ReasonButton';
import { useSelector } from 'react-redux';
import { symptomsConfigs } from '../../../store/symptoms/selectors';

interface SymptomSelectionProps {
  isOpen: boolean;
  closeFn: () => void;
}

const SymptomSelection: React.FC<SymptomSelectionProps> = ({ isOpen, closeFn }) => {
  const symptoms = useSelector(symptomsConfigs).symptoms;
  return (<Sheet isOpen={isOpen} onClose={closeFn} snapPoints={[800]}>
    <Sheet.Container>
      <Sheet.Header />
      <Sheet.Content>
        <Styled.Container>
          <Styled.ContentContainer>
            <Styled.Title>Symptoms Conditions</Styled.Title>
            <Styled.SubTitle>Please specify your symptoms :</Styled.SubTitle>
            <Styled.ExampleSelection>
              e.g Cough
            </Styled.ExampleSelection>
            <Styled.SelectedSymptomsText>Selected symptoms :</Styled.SelectedSymptomsText>
            <Styled.SelectedSymptomAndReasonContainer>
              {(symptoms || []).map((item, index) => {
                return item.isSelected && (<ReasonButton key={`${item.name}_${index}`} label={item.name} isSelected={item.isSelected} />)
              })
              }
            </Styled.SelectedSymptomAndReasonContainer>
            <Styled.ChooseYourSymptomsText>Choose your symptoms :</Styled.ChooseYourSymptomsText>
            <Styled.ChooseSymptomAndReasonContainer>
              {(symptoms || []).map((item, index) => {
                return !item.isSelected && (<ReasonButton key={`${item.name}_${index}`} label={item.name} isSelected={item.isSelected} />)
              })
              }
            </Styled.ChooseSymptomAndReasonContainer>
          </Styled.ContentContainer>
          <Button label='Done' onClick={closeFn} primary={false} />
        </Styled.Container>
      </Sheet.Content>
    </Sheet.Container>
    <Sheet.Backdrop />
  </Sheet>);
};

export default SymptomSelection;