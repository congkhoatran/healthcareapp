import React from 'react';
import * as Styled from './styles';
import { Row, Col } from "antd";
import { ClockCircleFilled, DownOutlined } from '@ant-design/icons'
import SymptomSelection from '../SymptomSelection';
import CalendarSelection from '../CalendarSelection';
import moment from 'moment';

const ReasonAndBookingTime: React.FC = () => {
  const [isOpenSymptoms, setOpenSymptoms] = React.useState(false);
  const [isCalendarBooking, setCalendarBooking] = React.useState(false);
  const [dateTimeBooked, setDateTimeBooked] = React.useState('');

  const today = moment().toDate();

  const [bookedDate, setBookedDate] = React.useState(moment(today).format('ddd MMM, DD YYYY'));
  const [bookedTime, setBookedTime] = React.useState('Now');

  const [showFirstTime, setShowFirstTime] = React.useState(true);

  const showSymptomSelection = () => {
    setOpenSymptoms((isOpenSymptoms) => !isOpenSymptoms);
  };

  const closeSymptomSelection = () => {
    setOpenSymptoms(false);
  };
  const showCalendarBooking = () => {
    setShowFirstTime(true);
    setCalendarBooking((isCalendarBooking) => !isCalendarBooking);
  };

  const closeCalendarBooking = () => {
    setCalendarBooking(false);
  };

  const selectedDateTime = (date: string, time: string) => {
    setCalendarBooking(false);
    if (date.trim().length > 0) {
      setBookedDate(date);
      setBookedTime(time);
      if (time === 'Now') {
        setDateTimeBooked('Now');
      } else {
        const shortDate = moment(Date.parse(date)).format('DD MMM');
        setDateTimeBooked(`${shortDate} ${time}`);
      }
    } else {
      setDateTimeBooked('Now');
    }
  }

  const showBookingSelection = () => {
    const haveBookingTime = dateTimeBooked.trim().length > 0 && dateTimeBooked !== 'Now';
    return (
      <Styled.TimeButton onClick={showCalendarBooking} haveBooking={haveBookingTime}>
        <ClockCircleFilled style={{ color: 'white', fontSize: '20px', padding: 5 }} />
        <Styled.NowText>{haveBookingTime ? dateTimeBooked : 'Now'}</Styled.NowText>
        <DownOutlined style={{ color: 'white', fontSize: '15px', padding: 2 }} />
      </Styled.TimeButton>
    );
  }

  return (
    <>
      <SymptomSelection isOpen={isOpenSymptoms} closeFn={closeSymptomSelection} />
      <CalendarSelection isOpen={isCalendarBooking} closeFn={closeCalendarBooking} selectDateFn={selectedDateTime} currentDate={bookedDate} currentTime={bookedTime} firstTime={ showFirstTime }/>
      <Styled.Container>
        <Row>
          <Col span={3}>
            <Styled.LookUp />
          </Col>
          <Col span={9}>
            <Styled.AddReasonText onClick={showSymptomSelection}>
              Add Reasons
            </Styled.AddReasonText>
          </Col>
          <Col span={12}>
            {showBookingSelection()}
          </Col>
        </Row>
      </Styled.Container>
    </>
  )
};

export default ReasonAndBookingTime as React.ComponentType;