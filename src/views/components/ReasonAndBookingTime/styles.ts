import styled from 'styled-components';
import LookUpIcon from '../../../images/look_up.png';

export const Container = styled.div`
  background-color: #ebf4fe;
  box-shadow: inset 5px 5px 5px 0px #d2dfef;
  height: 50px;
  margin: 20px 10px 0 10px;
  border-radius: 10px;
`

export const LookUp = styled.div`
  height: 25px;
  background-image: url(${LookUpIcon});
  background-size: contain;
  background-repeat: no-repeat;
  margin: 10px 0 0 20px;
`

export const AddReasonText = styled.p`
  font-size: 1.35em;
  font-weight: 600;
  font-family: sans-serif;
  color: #394656;
  padding: 10px;
  cursor: pointer;
`

export const TimeButton = styled.button`
  float: right;
  margin: 5px 15px 0 0;
  height: 40px;
  border: none;
  background-color: ${(props: { haveBooking: boolean }) => props.haveBooking ? '#52c8d0' : '#ecb257'};
  border-radius: 15px;
  cursor: pointer;
`

export const NowText = styled.span`
  font-size: 1.15em;
  font-weight: 600;
  font-family: sans-serif;
  margin-left: 5px;
  color: white;
`