import React from 'react';
import * as Styled from './styles';

interface IButtonProps {
  label: string;
  primary?: boolean;
  onClick?: () => void;
};

const Button: React.FC<IButtonProps> = ({ label, primary = true, onClick }) => (
  <Styled.Container>
    <Styled.Button onClick={onClick} primary={primary}>{label}</Styled.Button>
  </Styled.Container>
);

export default Button;