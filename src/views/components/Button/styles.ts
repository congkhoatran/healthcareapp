import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  height: 100px;
`

export const Button = styled.button`
  width: 95%;
  border: ${(props: { primary: boolean }) => props.primary ? 'none' : '1px solid #ffffff'};
  background-color: ${(props: { primary: boolean }) => props.primary ? '#52c8d0' : '#ffffff'};
  color: ${(props: { primary: boolean }) => props.primary ? '#ffffff' : '#52c8d0'};
  box-shadow: ${(props: { primary: boolean }) => props.primary ? 'inset 4px 3px 3px 0px #8e9398' : '0px 0px 3px 1px #8e9398'};
  height: 50px;
  border-radius: 10px;
  font-size: 1.35em;
  font-weight: 600;
  font-family: sans-serif;
  margin-top: 10px;
  cursor: pointer;
`