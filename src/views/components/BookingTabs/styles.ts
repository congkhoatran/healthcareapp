import styled, { css } from 'styled-components';

const BaseButton = css`
  font-size: 1.35em;
  font-weight: 600;
  font-family: sans-serif;
  width: 160px;
  height: 40px;
  border-radius: 10px;
  border: none;
`

const TabActive = css`
  background-color: #FFFFFF;
  color: #52c8d0;
  box-shadow: 0px 0px 3px 3px #e7e6e8;
`

const TabInActive = css`
  background-color: transparent;
  color: #aab2b8;
`

export const Container = styled.div`
  margin-top: 10px;
  justify-content: center;
  display: flex;
  
`

export const TabContainer = styled.div`
  width: 325px;
  border-radius: 15px;
  background-color: #ebf4fe;
  box-shadow: inset 5px 5px 5px 0px #d2dfef;
`

export const DoctorTab = styled.button`
  ${BaseButton}
  ${(props: { isActived: boolean }) => props.isActived ? TabActive : TabInActive}
`

export const VideoConsultTab = styled.button`
  ${BaseButton}
  margin-left: 5px;
  ${(props: { isActived: boolean }) => props.isActived ? TabActive : TabInActive}
`