
import React, { useState } from 'react';
import * as Styled from './styles';

import TabNames from '../../utilities/enum';

const BookingTabs: React.FC = () => {
  const [tabActived, setTabActive] = useState(TabNames.DOCTOR);

  const changeTab = (tab: string) => {
    setTabActive(tab);
  };

  return (
    <Styled.Container>
      <Styled.TabContainer>
        <Styled.DoctorTab isActived={tabActived === TabNames.DOCTOR} onClick={() => changeTab(TabNames.DOCTOR)}>Doctor</Styled.DoctorTab>
        <Styled.VideoConsultTab isActived={tabActived === TabNames.VIDEO} onClick={() => changeTab(TabNames.VIDEO)}>Video Consult</Styled.VideoConsultTab>
      </Styled.TabContainer>
    </Styled.Container>
  )
};

export default BookingTabs as React.ComponentType;