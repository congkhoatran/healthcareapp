
import React from 'react';
import * as Styled from './styles';
import { useHistory } from 'react-router-dom';

import { Row, Col } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';
import routers_string from '../../utilities/routers';

const BookingHeader: React.FC = () => {
  const history = useHistory();
  const backToHomePage = () => {
    history.push(routers_string.HOME);
  };

  return (
    <Styled.Container>
      <Row>
        <Col span={2}>
          <ArrowLeftOutlined onClick={backToHomePage} style={{ fontSize: '35px', color: '#52c8d0', padding: '12px 0 0 10px', cursor: 'pointer' }} />
        </Col>
        <Col span={16}>
          <Styled.Title>Book a doctor</Styled.Title>
        </Col>
        <Col span={6}>
          <Styled.BookingButton />
        </Col>
      </Row>
    </Styled.Container>
  )
};

export default BookingHeader as React.ComponentType;