import styled from 'styled-components';
import DoctorHeaderPhoneImage from '../../../images/doctor_headphone.png';


export const Container = styled.div`
  position: fixed;
  width: 100%;
  top: 0;
  background-color: white;
`

export const BackButton = styled.button`

`

export const Title = styled.p`
  font-size: 1.35em;
  color: #394656;
  padding: 15px 0 0 60px;
  font-weight: 600;
  font-family: sans-serif;
`

export const BookingButton = styled.div`
  height: 45px;
  width: 45px;
  background-image: url(${DoctorHeaderPhoneImage});
  background-size: contain;
  float: right;
  margin: 5px 10px 0 0;
`

