import React, { useState } from 'react';
import * as Styled from './styles';

import { CheckOutlined, PlusOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch } from '../../../store/store';
import { updateSymptom } from '../../../store/symptoms/symptomsSlice';
import { symptomsConfigs } from '../../../store/symptoms/selectors';

interface ReasonButtonProps {
  label: string;
  isSelected: boolean;
}

const ReasonButton: React.FC<ReasonButtonProps> = ({ label, isSelected }) => {
  const [updated, setUpdated] = useState(false);

  const dispatch: AppDispatch = useDispatch();
  let symptoms = useSelector(symptomsConfigs).symptoms;

  const onclick = (name: string) => {
    let newSymptoms = JSON.parse(JSON.stringify(symptoms));
    setUpdated(true);
    setTimeout(function() {
      for (let index = 0; index < newSymptoms.length; index++) {
        if (newSymptoms[index].name === name) {
          newSymptoms[index].isSelected = !newSymptoms[index].isSelected;
        }
      }
      dispatch(updateSymptom(newSymptoms));
    }, 200);
  };

  return (<Styled.SymptomAndReasonButton className={updated ? 'hide' : ''} isSelected={isSelected} onClick={() => onclick(label)}>
    {label}
    {isSelected ? <CheckOutlined style={{ marginLeft: 5 }} /> : <PlusOutlined style={{ marginLeft: 5 }} />}
  </Styled.SymptomAndReasonButton>);
};

export default ReasonButton;