import styled from 'styled-components';

export const SymptomAndReasonButton = styled.button`
  height: 50px;
  box-shadow: 0px 0px 2px 2px #e7e6e8;
  background-color: ${(props: { isSelected: boolean }) => props.isSelected ? '#52c8d0' : '#ffffff'};
  border: none;
  border-radius: 15px;
  font-size: 1.35em;
  font-weight: 600;
  font-family: sans-serif;
  color: ${(props: { isSelected: boolean }) => props.isSelected ? '#ffffff' : '#52c8d0'};
  margin-left: 20px;
  margin-top: 10px;
  padding: 0 5px 0 5px;
  white-space: nowrap;
  transition: opacity 0.2s;

  &.hide {
    opacity: 0;
  }
`