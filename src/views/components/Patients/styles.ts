import styled, { css } from 'styled-components';

const BasePatientButton = css`
  height: 50px;
  box-shadow: 0px 0px 3px 3px #e7e6e8;
  background-color: #FFFFFF;
  border: none;
  border-radius: 10px;
  font-size: 1.35em;
  font-weight: 600;
  font-family: sans-serif;
  color: #aab2b8;
  margin-left: 20px;
  padding: 0 5px 0 5px;
  white-space: nowrap;

  &:focus {
    border: 2px solid #52c8d0;
  }
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`

export const Title = styled.p`
  font-size: 1.35em;
  color: #394656;
  padding: 20px 0 0 10px;
  font-weight: 600;
  font-family: sans-serif;
`

export const PatientContainer = styled.div`
  display: flex;
  padding: 5px 20px 5px 10px;
  flex-wrap: nowrap;
  overflow-x: auto;
  overflow-y: hidden;
`

export const AddPatientsButton = styled.button`
  width: 100px;
  min-width: 100px;
  height: 50px;
  box-shadow: 0px 0px 3px 3px #e7e6e8;
  background-color: #FFFFFF;
  border: none;
  border-radius: 10px;
  font-size: 1.35em;
  font-weight: 600;
  font-family: sans-serif;
  color: #aab2b8;
`

export const PatientButton = styled.button`
  ${BasePatientButton}
`