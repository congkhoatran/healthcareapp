import React from 'react';
import * as Styled from './styles';

const Patients: React.FC = () => (
  <Styled.Container>
    <Styled.Title>Choose Patients</Styled.Title>
    <Styled.PatientContainer>
      <Styled.AddPatientsButton>
        Add
      </Styled.AddPatientsButton>
      <Styled.PatientButton>
        Yarik Nikolenko
      </Styled.PatientButton>
      <Styled.PatientButton>
        Julia Tela
      </Styled.PatientButton>
      <Styled.PatientButton>
        Yarik Nikolenko
      </Styled.PatientButton>
      <Styled.PatientButton>
        Julia Tela
      </Styled.PatientButton>

    </Styled.PatientContainer>
  </Styled.Container>
);

export default Patients as React.ComponentType;