import styled,{ css } from 'styled-components';

const BaseU = css`
  height: auto;
  overflow: auto;
  list-style-type: none;
  text-decoration: none;
  scroll-snap-type: y mandatory;
  z-index: 1;
  font-weight: 600;

  li {
    font-size: 1.35em;
    padding: 15px;
    height: 55px;
    min-width: 200px;
    scroll-snap-align: start;
    scroll-snap-stop: normal;
  }
`


export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: end;
  height: 90%;
`

export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 90%;
`

export const Title = styled.p`
  font-size: 1.35em;
  font-weight: bold;
  font-family: sans-serif;
  color: #394656;
  margin-bottom: 0;
  padding: 0 20px 0 20px;
`

export const SubTitle = styled.p`
  font-size: 1.35em;
  font-family: sans-serif;
  color: #394656;
  padding: 0 20px 0 20px;
`

export const ExampleSelection = styled.div`
  background-color: #ebf4fe;
  box-shadow: inset 5px 5px 5px 0px #d2dfef;
  height: 50px;
  border-radius: 10px;
  margin-bottom: 20px;
  padding: 10px;
  font-size: 1.35em;
  font-weight: 600;
  color: #b9c4d2;
`

export const CalendarContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  height: 75%;
  position: relative;
`

export const CenterLine= styled.div`
  border-top: 1px solid #52c8d0;
  border-bottom: 1px solid #52c8d0;
  width: 100%;
  position: absolute;
  height: 60px;
  margin-top: 108px;
`

export const DateList = styled.div`
  ${BaseU}
`

export const TimeList = styled.div`
  ${BaseU}
`

export const Item = styled.li`
  color: ${(props: { isSelected?: boolean }) => props.isSelected ? '#52c8d0' : '#aab2b8'};
`
