import React, { useCallback, useEffect, useState } from 'react';
import * as Styled from './styles';
import Sheet from 'react-modal-sheet';
import Button from '../Button';
import moment from 'moment';

interface CalendarSelectionProps {
  isOpen: boolean;
  closeFn: () => void;
  selectDateFn: (date: string, time: string) => void;
  currentDate: string;
  currentTime: string;
  firstTime: boolean;
}

const CalendarSelection: React.FC<CalendarSelectionProps> = ({ isOpen, closeFn, selectDateFn, currentDate, currentTime }) => {
  const today = moment().toDate();
  const [selectedDate, setSelectedDate] = useState(currentDate);
  const [selectedTime, setSelectedTime] = useState(currentTime);

  const [showFirstTime, setShowFirstTime] = useState(true);

  const [offsetDate, setOffsetDate] = useState(0);
  const [offsetTime, setOffsetTime] = useState(0);
  let days: moment.unitOfTime.DurationConstructor = 'days';
  const totalDays: number = 7;
  const totalTime: number = 24;

  let dates: any = []
  const bookingDates = () => {
    dates.push(<Styled.Item key={`empty1`}></Styled.Item>);
    dates.push(<Styled.Item key={`empty2`}></Styled.Item>);
    for (let index = 0; index <= totalDays; index++) {
      const nextDay = moment(today).add(index, days).toDate();
      const fullNextDay = moment(nextDay).format('ddd MMM, DD YYYY')
      const isSelect = fullNextDay === selectedDate;
      dates.push(<Styled.Item key={`${index}_${fullNextDay}`} isSelected={isSelect}>{fullNextDay}</Styled.Item>);
    }
    dates.push(<Styled.Item key={`empty3`}></Styled.Item>);
    dates.push(<Styled.Item key={`empty4`}></Styled.Item>);
    return dates;
  }

  const bookingTimes = () => {
    let items = [];
    items.push(<Styled.Item key={`empty1`}></Styled.Item>);
    items.push(<Styled.Item key={`empty2`}></Styled.Item>);
    items.push(<Styled.Item key={`now`} isSelected={selectedTime === 'Now'}>Now</Styled.Item>)

    for (let index = 0; index < totalTime; index++) {
      const time = `${index}:00 - ${index + 1}:00`;
      const isSelected = time === selectedTime;
      items.push(<Styled.Item key={`${index}_${time}`} isSelected={isSelected}>{time}</Styled.Item>);
    }
    items.push(<Styled.Item key={`empty3`}></Styled.Item>);
    items.push(<Styled.Item key={`empty4`}></Styled.Item>);
    return items;
  }

  const dateScrollEvent = useCallback((event) => {
    const scrollTop = event.target.scrollTop + 110;
    for (let index = 0; index < event.target.childNodes.length; index++) {
      if (event.target.childNodes[index].offsetTop > scrollTop - 5
        && event.target.childNodes[index].offsetTop < scrollTop + 5) {
        event.target.children[index].style.color = '#52c8d0';
        setOffsetDate(event.target.children[index].offsetTop);
        setSelectedDate(event.target.children[index].innerHTML);
      } else {
        event.target.children[index].style.color = '#aab2b8';
      }
    }
  }, []);

  const timeScrollEvent = useCallback((event) => {
    const scrollTop = event.target.scrollTop + 110;
    for (let index = 0; index < event.target.childNodes.length; index++) {
      if (event.target.childNodes[index].offsetTop > scrollTop - 5
        && event.target.childNodes[index].offsetTop < scrollTop + 5) {
        event.target.children[index].style.color = '#52c8d0';
        setOffsetTime(event.target.children[index].offsetTop);
        setSelectedTime(event.target.children[index].innerHTML);
      } else {
        event.target.children[index].style.color = '#aab2b8';
      }
    }
  }, []);

  const completeSelectedDateTime = () => {
    setShowFirstTime(true);
    selectDateFn(selectedDate, selectedTime);
  };

  useEffect(() => {
    return () => {
      if (dates.length > 0 && showFirstTime) {
        const dateList = document.getElementById('dateList');
        dateList?.scrollTo(0, offsetDate - 110);

        const timeList = document.getElementById('timeList');
        timeList?.scrollTo(0, offsetTime - 110);
        setShowFirstTime(false);
      }
    };
  })


  return (<Sheet isOpen={isOpen} onClose={closeFn} snapPoints={[500]}>
    <Sheet.Container>
      <Sheet.Header />
      <Sheet.Content>
        <Styled.Container>
          <Styled.ContentContainer>
            <Styled.Title>Schedule appointment</Styled.Title>
            <Styled.SubTitle>Please select date and time window:</Styled.SubTitle>
            <Styled.CalendarContainer>
              <Styled.CenterLine />
              <Styled.DateList id={'dateList'} onScroll={dateScrollEvent}>
                {bookingDates()}
              </Styled.DateList>
              <Styled.TimeList id={'timeList'} onScroll={timeScrollEvent}>
                {bookingTimes()}
              </Styled.TimeList>
            </Styled.CalendarContainer>
          </Styled.ContentContainer>
          <Button label='Done' onClick={completeSelectedDateTime} primary={true} />
        </Styled.Container>
      </Sheet.Content>
    </Sheet.Container>
    <Sheet.Backdrop />
  </Sheet>);
};

export default CalendarSelection;